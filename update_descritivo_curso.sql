UPDATE courses SET text = '<p style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif"><strong>O&nbsp;Curso para&nbsp;Taxista&nbsp;est&aacute; na modalidade online (EAD).&nbsp;</strong>Na <strong>LM Cursos de Tr&acirc;nsito</strong> voc&ecirc; encontra um conte&uacute;do online especializado para <strong>TAXISTA</strong>.</span></span></p>

<p style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif">A Lei 12.468/11 regulamenta a profiss&atilde;o de taxista e d&aacute; outras provid&ecirc;ncias.&nbsp;</span></span><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif">Ela reconhece a profiss&atilde;o e estabelece algumas normas em seus artigos sobre o transporte privativo de passageiros.&nbsp;&nbsp;</span></span></p>

<p style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif">Com um conte&uacute;do claro e objetivo, o aluno&nbsp;que optar por este curso ter&aacute; o esclarecimento sobre essa profiss&atilde;o e acessar&aacute; o conte&uacute;do regulamentado na Lei 12.468/11 e&nbsp;Resolu&ccedil;&atilde;o 456/13.</span></span></p>

<p style="text-align:center"><a href="https://api.whatsapp.com/send?phone=5521964150092&amp;text=Ol%C3%A1%2C%20Gostaria%20de%20Informa%C3%A7%C3%B5es%20sobre%20o%20Curso%20para%20Taxista" target="_blank"><img alt="" src="https://i.imgur.com/mmqL1mg.png" style="height:61px; width:350px" /></a></p>

<h2><strong><img alt="" src="https://i.imgur.com/C7Ol0uI.png" style="height:40px; width:40px" />&nbsp;<span style="color:#FF8C00"><span style="font-size:20px"><span style="font-family:tahoma,geneva,sans-serif">CARGA HOR&Aacute;RIA E PRAZO</span></span></span></strong><span style="color:#FF8C00"><span style="font-size:20px"><span style="font-family:tahoma,geneva,sans-serif">​</span></span></span></h2>

<ul>
    <li style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif">Curso com 40 horas-aula para o munic&iacute;pio do Rio de Janeiro;</span></span></li>
    <li style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif">Curso com 28 horas-aula para outros munic&iacute;pios;</span></span></li>
    <li style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif"><strong>Prazo m&iacute;nimo legal de conclus&atilde;o: 4 dias para o munic&iacute;pio do Rio de Janeiro;</strong></span></span></li>
    <li style="text-align:justify"><strong><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif">Prazo m&iacute;nimo legal de conclus&atilde;o: 3&nbsp;dias para outros munic&iacute;pios.</span></span></strong><strong><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif">​</span></span></strong></li>
</ul>

<p><img alt="" src="https://i.imgur.com/hZDqVad.png" style="height:40px; width:40px" />&nbsp;<span style="color:#FF8C00"><span style="font-size:20px"><span style="font-family:tahoma,geneva,sans-serif"><strong>FORMA DE PAGAMENTO</strong></span></span></span></p>

<ul>
</ul>

<p style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif">Pague &agrave; vista com boleto ou parcele em<strong> at&eacute; 10 vezes SEM JUROS</strong> no cart&atilde;o de cr&eacute;dito.&nbsp;Desconto especial para dep&oacute;sito banc&aacute;rio&nbsp;Entre em contato para saber mais informa&ccedil;&otilde;es.</span></span></p>

<p><img alt="" src="https://i.imgur.com/Q32wDq1.png" style="height:40px; width:40px" />&nbsp;<span style="color:#FF8C00"><span style="font-size:20px"><span style="font-family:tahoma,geneva,sans-serif"><strong>QUEM PODE FAZER O CURSO?</strong></span></span></span></p>

<ul>
    <li style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif">Motoristas (Titulares e Auxiliares) do Servi&ccedil;o de Transporte Individual de Passageiros em Ve&iacute;culos de Aluguel a Tax&iacute;metro;</span></span></li>
    <li style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif">Possuir a carteira nacional de habilita&ccedil;&atilde;o nas categorias B, C, D ou E.</span></span></li>
</ul>

<p><img alt="" src="https://i.imgur.com/PhtiwmB.png" style="height:40px; width:40px" />&nbsp;<span style="font-size:20px"><span style="color:#FF8C00"><span style="font-family:tahoma,geneva,sans-serif"><strong>FORMA DE REALIZA&Ccedil;&Atilde;O</strong></span></span></span></p>

<p style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif">Inteiramente a dist&acirc;ncia (EAD). Com acesso &agrave; internet voc&ecirc; poder&aacute; estudar usando um computador, tablet, notebook ou mesmo um smartphone.</span></span></p>

<p><img alt="" src="https://i.imgur.com/YWkJYni.png" style="height:50px; width:50px" /><span style="font-family:tahoma,geneva,sans-serif"><span style="color:#FF8C00"><span style="font-size:20px">&nbsp;<strong>CERTIFICADO&nbsp;</strong></span></span></span></p>

<p><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif">Para ser aprovado e obter seu certificado &eacute; exigido no m&iacute;nimo 50% de aprovaca&ccedil;&atilde;o no <strong>Curso para Taxista.</strong></span></span></p>

<p><span style="color:#FF8C00"><span style="font-size:20px"><span style="font-family:tahoma,geneva,sans-serif"><img alt="" src="https://i.imgur.com/Q1pMB89.png" style="height:40px; width:40px" />&nbsp;&nbsp;<strong>CONTE&Uacute;DO DO CURSO</strong></span></span></span></p>

<p style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif">Estude com conte&uacute;dos claros e din&acirc;micos, enriquecidos com v&iacute;deos e simulados dispon&iacute;veis 24 horas por dia.</span></span></p>

<p style="text-align:justify"><span style="color:#FF8C00"><span style="font-size:20px"><span style="font-family:tahoma,geneva,sans-serif"><strong><img alt="" src="https://i.imgur.com/zXbkvCx.png" style="height:40px; width:40px" />&nbsp;ACESSIBILIDADE</strong></span></span></span></p>

<p style="text-align:justify"><span style="color:#000000"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif">O curso conta com L&iacute;ngua Brasileira de Sinais - LIBRAS dispon&iacute;vel para auxiliar os deficientes auditivos.</span></span></span></p>

<p style="text-align:justify"><span style="color:#FF8C00"><span style="font-size:20px"><span style="font-family:tahoma,geneva,sans-serif"><strong><img alt="" src="https://i.imgur.com/frP52Qg.png" style="height:40px; width:40px" />CIDADES CREDENCIADAS</strong></span></span></span></p>

<p style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif"><strong>RIO DE JANEIRO:</strong> </span></span></p>

<p style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif"><strong>Rio de Janeiro</strong>;&nbsp;Duque de Caxias; Nova Igua&ccedil;u; Niter&oacute;i; Belford Roxo; Campos dos Goytacazes; S&atilde;o Jo&atilde;o de Meriti; Mag&eacute;; Itabora&iacute;; Cabo Frio; Angra dos Reis; Nova Friburgo; Teres&oacute;polis; Mesquita; Nil&oacute;polis; Maric&aacute;; Queimados; Rio das Ostras; Resende; Japeri; Araruama; Itagua&iacute;; S&atilde;o Pedro da Aldeia; Itaperuna; Barra do Pira&iacute;; Saquarema; Serop&eacute;dica; Tr&ecirc;s Rios; Valen&ccedil;a; Rio Bonito; Guapimirim; Cachoeiras de Macacu; Paracambi; Para&iacute;ba do Sul; Mangaratiba; Casimiro de Abreu; Paraty; Santo Ant&ocirc;nio de P&aacute;dua; S&atilde;o Francisco de Itabapoana; S&atilde;o Fid&eacute;lis;&nbsp;Vassouras; S&atilde;o Jo&atilde;o da Barra; Tangu&aacute;; B&uacute;zios; Arraial do Cabo; Pira&iacute;; Iguaba Grande; Paty do Alferes; Bom Jardim; Miracema; Miguel Pereira; Pinheiral; Quissam&atilde;; Itaocara; Concei&ccedil;&atilde;o de Macabu; Cordeiro; Silva Jardim; S&atilde;o Jos&eacute; do Vale do Rio Preto; Cantagalo; Carmo; Porci&uacute;ncula; Mendes; Sapucaia; Carapebus; Sumidouro; Cambuci; Natividade; Italva; Quatis; Engenheiro Paulo de Frontin; Cardoso; Moreira; Areal; Aperib&eacute;; Duas Barras; Trajano de Moraes; Santa Maria&nbsp;Madalena; S&atilde;o Sebasti&atilde;o do Alto; Rio das Flores; Laje do Muria&eacute;; S&atilde;o Jos&eacute; de Ub&aacute;; Macuco.</span></span></p>

<p style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif"><strong>S&Atilde;O PAULO:</strong> </span></span></p>

<p style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif">Sorocaba;&nbsp;&Aacute;gua Vermelha; Regin&oacute;polis; Sert&atilde;ozinho; S&atilde;o Jos&eacute; do Rio Preto; Campinas; Lorena; Taubate.</span></span></p>

<p style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif"><strong>MINAS GERAIS:</strong> </span></span></p>

<p style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif">Mariana; Mutum; Nova Era; Teixeiras.&nbsp;</span></span></p>

<p style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif"><strong>ESP&Iacute;RITO SANTO:</strong> </span></span></p>

<p style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif">Vila Velha; Pedro Can&aacute;rio.</span></span></p>

<p style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif"><strong>BAHIA: </strong></span></span></p>

<p style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif">Jacobina; Senhor do Bonfim.</span></span></p>

<p style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif"><strong>SERGIPE:</strong> </span></span></p>

<p style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif"><strong>Aracaju</strong>; Laranjeiras; Propri&aacute;; Barra dos Coqueiros; Sebasti&atilde;o; Laranjeiras .</span></span></p>

<p style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif"><strong>PERNAMBUCO: </strong></span></span></p>

<p style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif"><strong>Recife</strong>;<strong>&nbsp;</strong>Caruaru; S&atilde;o Bento do Una.</span></span></p>

<p style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif"><strong>MARANH&Atilde;O:</strong> </span></span></p>

<p style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif"><strong>S&atilde;o Lu&iacute;s</strong>; Balsas; Caxias; Pa&ccedil;o de Lumiar; Raposa; S&atilde;o Jos&eacute; de Ribamar.</span></span></p>

<p style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif"><strong>RIO GRANDE DO NORTE: </strong></span></span></p>

<p style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif"><strong>Natal</strong>;&nbsp;S&atilde;o Gon&ccedil;alo do Amarante; Mossoro; Extremoz.</span></span></p>

<p style="text-align:justify"><strong><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif">PIAU&Iacute;: </span></span></strong></p>

<p style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif">Piripiri.</span></span></p>

<p style="text-align:justify"><strong><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif">PAR&Aacute;: </span></span></strong></p>

<p style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif">Bel&eacute;m.</span></span></p>

<p style="text-align:justify"><strong><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif">RIO GRANDE DO SUL: </span></span></strong></p>

<p style="text-align:justify"><span style="font-size:16px"><span style="font-family:tahoma,geneva,sans-serif">Novo Hamburgo; Santana da Boa Vista.</span></span></p>

<p style="text-align:center"><a href="https://api.whatsapp.com/send?phone=5521964150092&amp;text=Ol%C3%A1%2C%20Gostaria%20de%20Informa%C3%A7%C3%B5es%20sobre%20o%20Curso%20para%20Taxista" target="_blank"><img alt="" src="https://i.imgur.com/mmqL1mg.png" style="height:61px; width:350px" /></a></p>

<p style="text-align:center"><span style="color:#27ae60"><span style="font-size:18px"><span style="font-family:tahoma,geneva,sans-serif"><strong>N&atilde;o achou sua cidade? </strong></span></span><span style="font-size:18px"><span style="font-family:tahoma,geneva,sans-serif"><strong>Entre em contato pelo WhatsApp:</strong></span></span></span></p>

<p style="text-align:center"><span style="color:#27ae60"><span style="font-size:24px"><span style="font-family:tahoma,geneva,sans-serif"><strong>(21)&nbsp;96415-0092</strong></span></span></span></p>
 ' WHERE id = 25;