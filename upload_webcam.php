<!-- Backend -->
<?php  
	define('UPLOAD_DIR', 'file/');
	$img = $_POST['base64image'];
	$img = str_replace('data:image/jpeg;base64,', '', $img);
	$img = str_replace(' ', '+', $img);
	$data = base64_decode($img);
	$file = UPLOAD_DIR . uniqid() . '.png';
	$success = file_put_contents($file,$data);
	print $success ? $file : 'Unable to save the file.';
?>

<!-- Frontend -->
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Tirar foto com camera</title>
</head>
<body>
	<button id="btnUm">Botão 1</button> <br>

	<div id="camera"></div>
	<div id="imagelist"></div>
	<div id="resultado"></div>

	<script src="js/webcam.min.js"></script>
	<script type="text/javascript">

		Webcam.set({
			width: 320,
			height: 240,
			image_format: 'jpeg',
			jpeg_quality: 90
		});
        Webcam.attach('#camera');

		window.onload = function(e){ 
			var btn = document.querySelector('#btnUm');

			btn.addEventListener("click", function(){

					Webcam.snap( function(data_uri) {

	
						var ajax;
						if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
						    ajax=new XMLHttpRequest();
						} else {// code for IE6, IE5
						    ajax=new ActiveXObject("Microsoft.XMLHTTP");
						}
						ajax.open("POST", "geraimagem.php", true);
						ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
						ajax.send("base64image="+data_uri);
						ajax.onreadystatechange = function() {
							if (ajax.readyState == 4 && ajax.status == 200) {
							  	var data = ajax.responseText;
							    document.getElementById('resultado').innerHTML = data;
							}
						}
						document.getElementById('imagelist').innerHTML = '<img src="'+data_uri+'"/>';

					});//Fim WebCam.snap
			});
		};
	</script>

</body>
</html>