SELECT * FROM vendas WHERE status = 1

CREATE TABLE pagamentos(
	id int(11) not null auto_increment,
	valor decimal(9,2),
	data_compra datetime,
	id_venda int(11) not null,
	constraint pk_pagamento primary key(id),
	INDEX venda_index (id_venda),
	constraint fk_vendas foreign key pagamentos(id_venda) references vendas(id) ON DELETE CASCADE
);

drop table pagamentos;


select * from pagamentos;
select * from vendas;

INSERT INTO pagamentos VALUES(null,9.20,now(),10);

delete from pagamentos;
delete from vendas;

insert into vendas values(null,0,0);

alter table vendas
add column laco int(11) null default 0;

alter table pagamentos
add INDEX venda_index (id_venda);



ALTER TABLE pagamentos
add constraint fk_vendas foreign key pagamentos(id_venda) references vendas(id) ON DELETE CASCADE;


INDEX par_ind (parent_id),
FOREIGN KEY (parent_id) REFERENCES parent(id)
ON DELETE CASCADE



CREATE TABLE clientes(
	/*
		Relacionamento 1:1 com a tabela documentos
		checar se entre duas opções no sexo
	*/
	idcliente int auto_increment,
	nome varchar(60) not null,
	sexo char(1) not null,
	status char(1) not null,
	CONSTRAINT pk_cliente PRIMARY KEY(idcliente),
	CONSTRAINT ck_cliente_sexo CHECK (sexo IN('F','M','f','m'))
);

CREATE TABLE documentos(
	/*
		Relacionamento 1:1 com a tabela documentos
		checar se entre duas opções no sexo
	*/
	iddocumento int not null auto_increment,
	idcliente int not null,
	tipodocumento varchar(25) not null,
	numerodocumento varchar(15) not null,
	CONSTRAINT pk_documento PRIMARY KEY(iddocumento),
	CONSTRAINT fk_idcliente FOREIGN KEY (idcliente) REFERENCES clientes (idcliente)
);

INSERT INTO clientes VALUES (null,'Bruno Richart','P',1);

SHOW VARIABLES LIKE 'auto_inc%';

CREATE TABLE telefones(
	/*
		Relacionamento 1:N com a tabela clientes
		um cliente pode ter mais de um telefone
	*/
	#1:N	
	idtelefone int not null auto_increment,
	idcliente int null,
	tipotelefone varchar(15) not null,
	numerotelefone varchar(15) not null,
	CONSTRAINT pk_telefone PRIMARY KEY(idtelefone),
	CONSTRAINT fk_telefone_cliente FOREIGN KEY (idcliente) REFERENCES clientes(idcliente)
);

SELECT * FROM telefones WHERE idcliente = 1;

CREATE TABLE pedidos(
	-- Pedido cliente se relacionam em um relacionamento N:N
	-- Clientes pode ter varios pedidos e pedidos tem varios clientes
	idpedido int not null auto_increment,
	datacompra datetime not null,
	valortotal decimal(9,2) not null,
	CONSTRAINT pk_pedido PRIMARY KEY(idpedido)
);

CREATE TABLE cliente_pedido(
	-- Relacionamento N:N gera uma terceira tabela sempre 
	idcliente int not null,
	idpedido int not null,
	CONSTRAINT pk_cliente_pedido PRIMARY KEY (idcliente,idpedido), -- Aqui é a chave composta
	CONSTRAINT fk_cliente_pedido FOREIGN KEY (idcliente) REFERENCES clientes (idcliente),
	CONSTRAINT fk_pedido_cliente FOREIGN KEY (idpedido) REFERENCES pedidos (idpedido)
);

INSERT INTO pedidos VALUES (null, now(),800.00);

INSERT INTO cliente_pedido VALUES (1,1);

SELECT * FROM cliente_pedido;


-- Deletando e atualizando em cascata
CREATE TABLE parent (
    id INT NOT NULL,
    PRIMARY KEY (id)
) ENGINE=INNODB;

CREATE TABLE child (
        id INT, 
        parent_id INT,
        INDEX par_ind (parent_id),
        FOREIGN KEY (parent_id) 
            REFERENCES parent(id)
            ON UPDATE CASCADE ON DELETE CASCADE
    ) ENGINE=INNODB;


INSERT INTO parent VALUES (1);
INSERT INTO child VALUES (1,1);
INSERT INTO child VALUES (2,1);
